import {html, render} from '../../lit-html/lit-html.js';

function getEvents() {
    return fetch('/api/events')
    .then(function(response) {
      return response.json();
    })
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

(async function() {
    let events = Object.values((await getEvents()).data);
    console.log(events);
    let content = html`${events.map((event) => html`
    <div class="well">
        <a href="event.html" class="text-center">${event.name} : </a><br>
        <p class="text-center">${formatDate(event.date)}<p>
    </div>
    `)}`
    render(html`${content}`, document.getElementById("events"));        

})();