import {html, render} from '../../lit-html/lit-html.js';

function getEvents() {
    return fetch('/api/events')
    .then(function(response) {
      return response.json();
    })
}

(async function() {
    let events = Object.values((await getEvents()).data);
    let content = html`${events.map((event) => html`
    <div class="col-sm-3">
        <p>${event.name}</p>
        <a style="width:100%"><img src="${event.image_path}" class="img-responsive" style="width:100%" alt="Image"></a>
    </div>
    `)}`
    render(html`${content}`, document.getElementById("events"));        

})();