const mongoose = require('mongoose')

let eventSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    image_path: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Events', eventSchema)