const express = require('express')
const app = express()
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const api_routes = require("./api/index.js");
const Task = require("./models/events.js")
const config = require('./config/config.js');
const fs = require('fs');

// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

app.use(/^\/api/, api_routes);

app.use(express.static('front'))

global.db = mongoose.connect(global.gConfig.dburi, { useNewUrlParser: true }).then(() => {
    console.log("Mongo connected !");
    if (!fs.existsSync(global.gConfig.dir)){
        fs.mkdirSync(global.gConfig.dir);
    }    
    return app.listen(global.gConfig.port, () => console.log(`Epiviosi app listening on port ${global.gConfig.port}!`));
})
.catch(err => { // mongoose connection error will be handled here
    console.error('App starting error:', err.stack);
    process.exit(1);
});