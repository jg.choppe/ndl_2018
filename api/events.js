const express = require("express");
const Events = require('../models/events.js');
const multer = require('multer');
const config = require("../config/config.js");

const router = express.Router({ caseSensitive: true });
const upload = multer({dest: global.gConfig.dir});


router.get(/^\/?/, async (req, res) => {
    try {
        Events.find({}, function(err, events) {
            if (err)
                throw {err: "Invalid find"};
            var eventsMap = {};
        
            events.forEach(function(event) {
                let path = event.image_path.split("/")
                path.shift();
                event.image_path = path.join("/");
                eventsMap[event ._id] = event;
            });
        
            res.json({ success: "ko", data: eventsMap});  
        });
    } catch (err) {
        console.log(err);
        res.status(400).json({ success: "ko", err: JSON.stringify(err) });
    }
});

router.post(/^\/?/, upload.single("image"), async (req, res) => {
    try {
        console.log("event creation");
        let event = new Events({
            name: req.body.name,
            image_path: req.file.path,
            description: req.body.description,
            date: new Date(req.body.date)
        });
        event.save()
        res.json({success: "ok", data: {}});
    } catch (err) {
        console.log(err);
        res.status(400).json({success: "ko", err: JSON.stringify(err) });
    }
});

router.delete(/^(.+)\/?/, async (req, res) => {
    try {
        Events.findOneAndDelete({name: req.body.name})
            .then((rep) => {
                res.json({success: "ok"});
            })
            .catch((err) => {
                res.status(400).json({ success: "ko", err: JSON.stringify(err) });
        });
    } catch (err) {
        console.log(err);
        res.status(400).json({ success: "ko", err: JSON.stringify(err) });
    }
});

router.use(async (req, res) => {
    res.status(400).json({ success: "ko", err: "Endpoint not found." });
});

module.exports = router;