const express = require("express");
const tasks_routes = require("./events.js");

const router = express.Router({ caseSensitive: true });

router.use(/^\/events/, tasks_routes);

router.use(async (req, res) => {
    res.status(400).json({ type: "error", reason: "Endpoint not found." });
});

module.exports = router;