
const config = {
    dburi: process.env.SCALINGO_MONGO_URL || "mongodb://user:password@localhost:27017/mydb",
    port: process.env.PORT || 8080,
    dir: process.env.FILES || "front/files"
}

global.gConfig = config;