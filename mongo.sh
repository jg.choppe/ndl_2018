# !/bin/bash

mkdir data 2> /dev/null
docker run --name mongo \
  -p 27017:27017 \
  -v `pwd`/data:/bitnami \
  -e MONGODB_USERNAME=user -e MONGODB_PASSWORD=password \
  -e MONGODB_DATABASE=mydb -d bitnami/mongodb:latest
